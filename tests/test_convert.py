import unittest
from pathlib import Path

import pandas as pd

from address2latlong.core import Address2LatLongConverter, LatLongResult

df = pd.read_csv(Path(".") / "data" / "latest.csv")
latlongs = [LatLongResult.from_geolonia(row) for idx, row in df.iterrows()]


class TestAdd(unittest.TestCase):
    converter: Address2LatLongConverter

    @classmethod
    def setUpClass(self) -> None:
        # auto save pickle, idempotent operation
        pickle_path = Path(".") / "tmp" / "trie.pickle"
        self.converter = Address2LatLongConverter(
            data_path=Path(".").parent.parent / "data" / "latest.csv",
            pickle_path=pickle_path,
        )
        if not pickle_path.exists():
            self.converter.pickle_save()

    @unittest.skip("need to update and temporally skip")
    def test_database_compatibility(self) -> None:
        self.assertEqual(len(self.converter.df), len(list(self.converter.tries.all_latlongs)))  # type: ignore
        """
        regarding geolonia(v0.5.0), following addresses seem duplicate
        長野県上伊那郡宮田村新田
        長野県上伊那郡宮田村南割
        長野県上伊那郡宮田村北割
        長野県上伊那郡宮田村中越
        長野県上伊那郡宮田村大久保
        愛媛県八幡浜市新川
        愛媛県八幡浜市松蔭町
        愛媛県八幡浜市北大黒町
        愛媛県八幡浜市昭和通
        愛媛県八幡浜市沖新田
        愛媛県八幡浜市南大黒町
        愛媛県八幡浜市幸町
        愛媛県八幡浜市大門
        愛媛県八幡浜市海望園
        愛媛県八幡浜市愛宕
        """

    def test_exawizards_company(self) -> None:
        ret = self.converter.latlong("東京都港区東新橋1丁目9-2")
        self.assertIsNotNone(ret)

    def test_exawizards_company_zenkaku(self) -> None:
        ret = self.converter.latlong("東京都港区東新橋１丁目9-2")
        self.assertIsNotNone(ret)

    def test_exawizards_company_with_space(self) -> None:
        ret = self.converter.latlong("東京 都港区　東新橋   1丁目9-2")
        self.assertIsNotNone(ret)

    def test_exawizards_company_fake1(self) -> None:
        ret = self.converter.latlong("東京都港区東新橋50丁目9-2")
        self.assertIsNone(ret)

    def test_exawizards_company_fake2(self) -> None:
        ret = self.converter.latlong("東京都港区東古橋1丁目9-2")
        self.assertIsNone(ret)

    def test_load_Result(self) -> None:
        input_latlong = latlongs

        ret = self.converter.latlong(
            [
                latlong["都道府県名"] + latlong["市区町村名"] + latlong["丁目名"] + latlong["通称名"]
                for latlong in input_latlong
            ]
        )
        self.assertListEqual(
            [
                latlong["都道府県名"] + latlong["市区町村名"] + latlong["丁目名"] + latlong["通称名"]
                for latlong in input_latlong
            ],
            [
                latlong["都道府県名"] + latlong["市区町村名"] + latlong["丁目名"] + latlong["通称名"]
                for latlong in ret
            ],
        )
