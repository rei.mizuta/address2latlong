import pickle
import re
import sys
import unicodedata
from decimal import Decimal
from pathlib import Path
from typing import Optional, TypedDict, Union

import pandas as pd
from kanjize import int2kanji

from address2latlong.interface import LatLongResult
from address2latlong.trie import Trie


class Address2LatLongConverter:
    def __init__(
        self,
        data_path: Optional[Path] = None,
        pickle_path: Optional[Path] = None,
    ):
        if not (pickle_path or data_path):
            raise ValueError("set latitute and longitude data path or pickle path")
        self.pickle_path = pickle_path  # for save
        if pickle_path and pickle_path.exists():
            print("pickle exists!", file=sys.stderr)
            with open(pickle_path, "rb") as f:
                self.trie = pickle.load(f)
        else:
            print("start reading csv...", file=sys.stderr)
            self.df = pd.read_csv(data_path)
            print("construct trie", file=sys.stderr)
            self.trie = Trie[LatLongResult](
                LatLongResult.from_geolonia(row) for idx, row in self.df.iterrows()
            )

    def pickle_save(self):
        if self.pickle_path:
            self.pickle_path.parent.mkdir(parents=True, exist_ok=True)
            with open(self.pickle_path, "wb") as f:
                pickle.dump(self.trie, f)

    def __latlong_internal(
        self, list_address: list[str]
    ) -> list[Optional[LatLongResult]]:
        # １２３ -> 123
        normalized_addresses = map(
            lambda address: unicodedata.normalize("NFKC", address), list_address
        )
        # eliminate spaces
        normalized_addresses = map(
            lambda address: re.sub(r"[\u3000 \t]", "", address), normalized_addresses
        )

        # 12丁目 -> 十二丁目
        def int2kanji_chome(address: str) -> str:
            search_result = re.search(r"[0-9]+丁目", address)
            return (
                address.replace(
                    search_result[0], int2kanji(int(search_result[0][:-2])) + "丁目"
                )
                if search_result
                else address
            )

        normalized_addresses = list(map(int2kanji_chome, normalized_addresses))
        return_values = [self.trie.search(address) for address in normalized_addresses]
        return return_values

    def latlong(
        self, address: Union[str, list[str]]
    ) -> Union[Optional[LatLongResult], list[Optional[LatLongResult]]]:
        if isinstance(address, str):
            ret_list = self.__latlong_internal([address])
            return ret_list[0]
        else:
            return self.__latlong_internal(address)
