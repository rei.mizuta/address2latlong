import unicodedata
from decimal import Decimal
from typing import TypedDict

import pandas as pd


class LatLongResult(TypedDict):
    都道府県名: str
    市区町村名: str
    大字町丁目名: str
    緯度: Decimal
    経度: Decimal

    @classmethod  # type: ignore
    def from_geolonia(self, row: pd.Series) -> "LatLongResult":  # type: ignore
        return {
            "都道府県名": row["都道府県名"],
            "市区町村名": row["市区町村名"],
            "丁目名": unicodedata.normalize(
                "NFKC", "" if row["大字町丁目名"] == "（大字なし）" else row["大字町丁目名"]
            ),
            "通称名": "" if row.isnull()["小字・通称名"] else row["小字・通称名"],
            "緯度": Decimal(row["緯度"]),
            "経度": Decimal(row["経度"]),
        }

    @classmethod  # type: ignore
    def name_from_latlong(self, latlong: "LatLongResult") -> str:  # type: ignore
        return latlong["都道府県名"] + latlong["市区町村名"] + latlong["丁目名"] + latlong["通称名"]  # type: ignore

    @classmethod  # type: ignore
    def is_address_startwith(self, address: str, latlong: "LatLongResult") -> bool:  # type: ignore
        return address.startswith(latlong["都道府県名"] + latlong["市区町村名"] + latlong["丁目名"])  # type: ignore
