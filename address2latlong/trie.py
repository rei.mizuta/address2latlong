import sys
from dataclasses import dataclass
from typing import Any, Generic, Optional, TypeVar

from address2latlong.interface import LatLongResult

T = TypeVar("T")


@dataclass
class TrieNone(Generic[T]):
    children: dict[str, "TrieNone[T]"]
    value: Optional[T]


class Trie(Generic[T]):
    def __init__(self, latlongs):
        self.root = self.__create_node(latlongs, 0)

    def __create_node(self, sub_latlongs, depth: int) -> TrieNone[T]:
        latlongs_with_name_and_prefix: list[Any, str, str] = list(
            (
                latlong,
                LatLongResult.name_from_latlong(latlong),
                LatLongResult.name_from_latlong(latlong)[depth]
                if len(LatLongResult.name_from_latlong(latlong)) > depth
                else "",
            )
            for latlong in sub_latlongs
        )
        value = None
        for latlong, name, prefix in latlongs_with_name_and_prefix:
            if not prefix:
                if value:
                    # raise ValueError(f"ambiguous key {value} and {latlong}")
                    print(f"duplicate key {value} and {latlong}", file=sys.stderr)
                value = latlong

        prefixes = list(
            set(tup[2] for tup in latlongs_with_name_and_prefix if tup[2] != "")
        )
        return TrieNone[T](
            children={
                prefix: self.__create_node(
                    list(
                        map(
                            lambda tup: tup[0],
                            filter(
                                lambda x: x[2] == prefix, latlongs_with_name_and_prefix
                            ),
                        )
                    ),
                    depth + 1,
                )
                for prefix in prefixes
            },
            value=value,
        )

    def search(self, key: str, node: Optional[TrieNone[T]] = None) -> Optional[T]:

        if node is None:
            node = self.root
        if not key:
            return node.value
        head = key[0]
        tail = key[1:]
        if node.children.get(head):
            return self.search(tail, node.children[head])
        else:
            return node.value
