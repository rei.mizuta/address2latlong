# address2latlong

converter from Japanese addresses to (latitude, longitude)

## prerequisite

download csv data from [geolonia](https://geolonia.github.io/japanese-addresses/)

e.g.

```bash
curl https://raw.githubusercontent.com/geolonia/japanese-addresses/master/data/latest.csv > data/latest.csv
```

Note: this data is published by [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.ja)

## TODO

- create docstring
- mypy のエラーを何とかする
- support normalize "~丁目" expression
  - e.g. "東京都港区東新橋 1-9-2" -> "東京都港区東新橋 1 丁目 9-2"
- trie 木など諸々のモジュールを抽象化したい
- duplicate key となるデータの原因把握
- kanjize を third_party 等のフォルダに移すか自前で実装する（個人メンテされている OSS であり、持続性の観点から）
